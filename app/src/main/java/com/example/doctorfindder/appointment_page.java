package com.example.doctorfindder;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.util.Calendar;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class appointment_page extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    Spinner hosptspneer,doctornamel;
    LinearLayout layoutfrist,layoutsecend,layoutthird;
    Animation uptodown,downtoup,getUptodown,getDowntoup;
    TextView textView,doctorTimeset;
    EditText specialityl,namea,phonea,agea,addressa;
    Button date1,datepic,next2a,next3a;
    RadioGroup gender;
    String gendervalue;
    Button submitbtn;
    TimePickerDialog timePickerDialog;
    DatabaseReference databaseReference,Reference;
    FirebaseDatabase database;
    private FirebaseAuth mAuth;
    private ProgressDialog progressBar;
    String hospitalname;
    String doctornamels;
    public List<String> catagory;
    public List<String> drcatagory;
    public ArrayAdapter<String> adapter;
    public ArrayAdapter<String> adapter2;
    String drEmail;
    String format;
    Calendar calendar;
    TimePickerDialog timepickerdialog;
    private int CalendarHour, CalendarMinute;
     @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_page);
         layoutfrist=(LinearLayout)findViewById(R.id.layout1);
         layoutsecend=(LinearLayout)findViewById(R.id.layout2);
         layoutthird=(LinearLayout)findViewById(R.id.layout3);


         //layout1
         specialityl=(EditText)findViewById(R.id.speciality);
         hosptspneer=(Spinner)findViewById(R.id.hospitalspneer);
         doctornamel=(Spinner)findViewById(R.id.doctorname);
         next3a=(Button)findViewById(R.id.next1);
         //layout2
         doctorTimeset=(TextView)findViewById(R.id.doctorTime);
         date1=(Button)findViewById(R.id.timep);
         datepic=(Button)findViewById(R.id.datepick);
         next2a=(Button)findViewById(R.id.next2);

         //layout3
         namea=(EditText)findViewById(R.id.name);
         phonea=(EditText)findViewById(R.id.phone);
         agea=(EditText)findViewById(R.id.age);
         addressa=(EditText)findViewById(R.id.address);
         gender=(RadioGroup)findViewById(R.id.radioGroup1);
         submitbtn=(Button)findViewById(R.id.submit);

         mAuth = FirebaseAuth.getInstance();
         database=FirebaseDatabase.getInstance();
         //reference=database.getReference("Appointment");
         databaseReference= FirebaseDatabase.getInstance().getReference("DrProfile");
         Reference= FirebaseDatabase.getInstance().getReference("Appointment");
         progressBar = new ProgressDialog(this);

         layoutfrist.setVisibility(View.VISIBLE);
         uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
         downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
         layoutfrist.setAnimation(uptodown);

         /*List<String> catagory=new ArrayList<>();
         catagory.add(0,"");
         catagory.add("Apollo Hospitals");
         catagory.add("United Hospital");
         catagory.add("Square Hospital");
         catagory.add("Apollo Hospital");
         catagory.add("Labaid Hospital");
         catagory.add("Ibn Sina Hospital");
         ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,catagory);
         //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         hosptspneer.setAdapter(adapter);*/
         spneershow();

         catagory= new ArrayList<String>();
         catagory.add(0,"");
         drcatagory= new ArrayList<String>();
         drcatagory.add(0,"");

         adapter=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,catagory);
         adapter2=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,drcatagory);




         hosptspneer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

              hospitalname=parent.getItemAtPosition(position).toString();
                 Query query=databaseReference.orderByChild("hospitalad").equalTo(hospitalname);
                 query.addValueEventListener(new ValueEventListener() {
                     @Override
                     public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                         drcatagory.clear();
                         for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                             DrShowClass inserty=dataSnapshot1.getValue(DrShowClass.class);
                             //final String[] areas = {inserty.getHospitalad()};
                             String aa=inserty.getDRname();
                             //Toast.makeText(appointment_page.this, "ggg"+aa, Toast.LENGTH_SHORT).show();

                             //catagory=new ArrayList<>();
                             drcatagory.add(aa);


                             //areasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                         }
                         doctornamel.setAdapter(adapter2);
                     }

                     @Override
                     public void onCancelled(@NonNull DatabaseError databaseError) {

                     }
                 });

             }

             @Override
             public void onNothingSelected(AdapterView<?> parent) {

             }
         });
         doctornamel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 doctornamels=parent.getItemAtPosition(position).toString();
                 Query query=databaseReference.orderByChild("DRname").equalTo(doctornamels);
                 query.addValueEventListener(new ValueEventListener() {
                     @Override
                     public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                         for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                             DrShowClass inserty=dataSnapshot1.getValue(DrShowClass.class);
                             //final String[] areas = {inserty.getHospitalad()};
                             String aa=inserty.getTime();
                              drEmail=inserty.getEmail();
                             doctorTimeset.setText("DR.Time :"+aa);
                             //Toast.makeText(appointment_page.this, "ggg"+aa, Toast.LENGTH_SHORT).show();


                         }

                     }

                     @Override
                     public void onCancelled(@NonNull DatabaseError databaseError) {

                     }
                 });

             }

             @Override
             public void onNothingSelected(AdapterView<?> parent) {

             }
         });

         date1.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 java.util.Calendar calendar= java.util.Calendar.getInstance();
                 //calendar = Calendar.getInstance();
                 CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
                 CalendarMinute = calendar.get(Calendar.MINUTE);


                 timepickerdialog = new TimePickerDialog(appointment_page.this,
                         new TimePickerDialog.OnTimeSetListener() {

                             @Override
                             public void onTimeSet(TimePicker view, int hourOfDay,
                                                   int minute) {

                                 if (hourOfDay == 0) {

                                     hourOfDay += 12;

                                     format = "AM";
                                 }
                                 else if (hourOfDay == 12) {

                                     format = "PM";

                                 }
                                 else if (hourOfDay > 12) {

                                     hourOfDay -= 12;

                                     format = "PM";

                                 }
                                 else {

                                     format = "AM";
                                 }


                                 date1.setText(hourOfDay + ":" + minute  +  format);
                             }
                         }, CalendarHour, CalendarMinute, false);
                 timepickerdialog.show();

             }
         });

         gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(RadioGroup group, int checkedId) {
                 switch(checkedId) {

                     case R.id.radio0:
                         gendervalue="Male";
                         break;
                     case R.id.radio1:
                         gendervalue="Female";
                         break;

                     case R.id.radio2:
                         gendervalue="Other";
                         break;
                 }

             }
         });
         datepic.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 dateshow();


             }
         });



    }




    public void next1(View view) {
      String specialityvalu=specialityl.getText().toString();
      //String hospitala=hosptspneer.getSelectedItem().toString();
      //String drNAme=doctornamel.getSelectedItem().toString();
      if (specialityvalu.isEmpty()){
          specialityl.setError("speciality Empty");
          specialityl.requestFocus();
       }
      /*if (hosptspneer.getSelectedItemPosition()==0){

          Toast.makeText(getApplication(),"Select hospital Name",Toast.LENGTH_LONG).show();

      }
        if (doctornamel.getSelectedItemPosition()==0){

            Toast.makeText(getApplication(),"Select DR Name",Toast.LENGTH_LONG).show();

        }*/
        else {

        layoutsecend.setVisibility(View.VISIBLE);
        getDowntoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        layoutsecend.setAnimation(getDowntoup);
        layoutfrist.setVisibility(View.GONE);
        }
    }

    public void next2(View view) {
         layoutthird.setVisibility(View.VISIBLE);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.push_left_out);
        layoutthird.setAnimation(downtoup);
        layoutsecend.setVisibility(View.GONE);
    }

    public void next3(View view) {
        String named=namea.getText().toString();
        String Hospitaln=hosptspneer.getSelectedItem().toString();
        String DrnaMe=doctornamel.getSelectedItem().toString();
        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage("Name: "+named+"\n"+"Dr Name :"+DrnaMe+"\n"+"Hospital :"+Hospitaln)
                .setTitle("Do you want to Appointment")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        addAppointment();



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(appointment_page.this,MainActivity.class));
            customType(appointment_page.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String daty= +dayOfMonth+"/"+(month+1)+"/"+year;
        datepic.setText(daty);
    }

    public void dateshow(){

        DatePickerDialog datePickerDialog=new DatePickerDialog(
                this,this,
                java.util.Calendar.getInstance().get(Calendar.YEAR),
                java.util.Calendar.getInstance().get(Calendar.MONTH),
                java.util.Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        );
        datePickerDialog.show();
    }

    public void spneershow() {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                catagory.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    DrShowClass inserty=dataSnapshot1.getValue(DrShowClass.class);
                    //final String[] areas = {inserty.getHospitalad()};
                    String aa=inserty.getHospitalad();

                     catagory.add(aa);
                    //Toast.makeText(appointment_page.this, "ggg"+aa, Toast.LENGTH_SHORT).show();


                    //areasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                }
                hosptspneer.setAdapter(adapter);





            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void addAppointment(){

      String speciality=specialityl.getText().toString();
      String doctorTime=doctorTimeset.getText().toString();
      String timed=date1.getText().toString();
      String dated=datepic.getText().toString();
      String named=namea.getText().toString();
      String aged=agea.getText().toString();
      String addressd=addressa.getText().toString();
      String phpned=phonea.getText().toString();
       String Hospitaln=hosptspneer.getSelectedItem().toString();
        String smsUId=Reference.push().getKey();


      String DrnaMe=doctornamel.getSelectedItem().toString();
        HashMap<Object,String> hashMap=new HashMap<>();
        hashMap.put("speciality",speciality);
        hashMap.put("doctorTime",doctorTime);
        hashMap.put("Admissiontime",timed);
        hashMap.put("Admissiondate",dated);
        hashMap.put("Name",named);
        hashMap.put("Age",aged);
        hashMap.put("address",addressd);
        hashMap.put("phone",phpned);
        hashMap.put("Hospital",Hospitaln);
        hashMap.put("Dr_name",DrnaMe);
        hashMap.put("Gender",gendervalue);
        hashMap.put("Uid",smsUId);
        hashMap.put("Email",drEmail);


        Reference.child(named).setValue(hashMap);
        submitbtn.setText("Appointment Successfully");
        Toast.makeText(this, "Appointment is Successfully", Toast.LENGTH_SHORT).show();


    }

}
