package com.example.doctorfindder;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class appointadptar extends ArrayAdapter <appointment_ShowClass> {
    public Activity context;
    public List<appointment_ShowClass> inlist;
    public appointadptar(Activity context, List<appointment_ShowClass> inlist) {
        super(context, R.layout.appoint_simple_layout, inlist);
        this.context = context;
        this.inlist = inlist;
    }


    @Override
    public View getView (int position, View convertView, ViewGroup parent)   {

        LayoutInflater layoutInflater=context.getLayoutInflater();

        View view=layoutInflater.inflate(R.layout.appoint_simple_layout,null,true);
        appointment_ShowClass insdfert=inlist.get(position);
        TextView apName=(TextView) view.findViewById(R.id.namedp);
        TextView address=(TextView) view.findViewById(R.id.addressdp);
        TextView phonenu=(TextView) view.findViewById(R.id.numberdp);
        TextView ap_age=(TextView) view.findViewById(R.id.agedp);
        TextView hospitaldp=(TextView) view.findViewById(R.id.hospitaldp);
        TextView gender_ap=(TextView) view.findViewById(R.id.genderdp);
        TextView time_ap=(TextView) view.findViewById(R.id.timedp);
        TextView date_ap=(TextView) view.findViewById(R.id.datedp);
        TextView speciality=(TextView) view.findViewById(R.id.specialitydp);


        apName.setText(""+insdfert.getName());
        address.setText(""+insdfert.getAddress());
        phonenu.setText(""+insdfert.getPhone());
        ap_age.setText("Age : "+insdfert.getAge());

        hospitaldp.setText(""+insdfert.getHospital());
        gender_ap.setText("Gender : "+insdfert.getGender());
        time_ap.setText(""+insdfert.getAdmissiontime());
        date_ap.setText(""+insdfert.getAdmissiondate());
        speciality.setText("Speciality : " +insdfert.getSpeciality());


        return view;
    }
}
