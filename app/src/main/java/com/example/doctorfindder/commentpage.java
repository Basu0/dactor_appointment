package com.example.doctorfindder;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class commentpage extends AppCompatActivity {
 EditText drnamet,commentt;
 Button button;
 RatingBar ratingBar;
    DatabaseReference databaseReference;
    FirebaseDatabase database;
    TextView tvRateMessage;
    private float ratedValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commentpage);
        drnamet=(EditText)findViewById(R.id.drname);
        commentt=(EditText)findViewById(R.id.comment);
        ratingBar=(RatingBar)findViewById(R.id.rating);
        button=(Button)findViewById(R.id.submit);
        tvRateMessage = (TextView) findViewById(R.id.tvRateMessage);
        databaseReference= FirebaseDatabase.getInstance().getReference("Rating");

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override

            public void onRatingChanged(RatingBar ratingBar, float rating,

                                        boolean fromUser) {

                ratedValue = ratingBar.getRating();


                if(ratedValue<1){

                    tvRateMessage.setText("ohh ho...");

                }else if(ratedValue<2){

                    tvRateMessage.setText("Ok.");

                }else if(ratedValue<3){

                    tvRateMessage.setText("Not bad.");

                }else if(ratedValue<4){

                    tvRateMessage.setText("Nice");

                }else if(ratedValue<5){

                    tvRateMessage.setText("Very Nice");

                }else if(ratedValue==5){

                    tvRateMessage.setText("Thank you..!!!");

                }

            }

        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String drNAme=drnamet.getText().toString();
                final String comment=commentt.getText().toString();
                final float rating=ratingBar.getRating();
                final String fullmass="Dr Name: "+drNAme+"\n"+"Comment :"+comment+"\n"+"Rating :"+rating;
                if (drNAme.isEmpty()){
                    drnamet.setError("Name Null");
                    drnamet.requestFocus();

                }
                if (comment.isEmpty()){
                    commentt.setError("Comment Null");
                    commentt.requestFocus();

                }else {
                android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(commentpage.this)
                        .setMessage("Dr Name: "+drNAme+"\n"+"Comment :"+comment+"\n"+"Rating :"+rating)
                        .setTitle("Do you want to Appointment")

                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            // do something when the button is clicked
                            public void onClick(DialogInterface arg0, int arg1) {

                                addrating();
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("message/html");
                                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"rakibhabibee@gmail.com"});
                                i.putExtra(Intent.EXTRA_SUBJECT, "Feedback from App");
                                i.putExtra(Intent.EXTRA_TEXT, " Dr Name : "+drNAme+"\nComment : "+comment+"\nRating :" +rating);
                                try {
                                    startActivity(Intent.createChooser(i, "Send feedback..."));
                                } catch (android.content.ActivityNotFoundException ex) {
                                    Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                                }



                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {

                            // do something when the button is clicked
                            public void onClick(DialogInterface arg0, int arg1) {
                            }
                        })
                        .show();}
            }
        });

    }

    public void addrating(){
       String drNAme=drnamet.getText().toString();
       String comment=commentt.getText().toString();
       float rating=ratingBar.getRating();
        String smsUId=databaseReference.push().getKey();

        if (drNAme.isEmpty()){
            drnamet.setError("Name Null");
            drnamet.requestFocus();

        }
        if (comment.isEmpty()){
            commentt.setError("Comment Null");
            commentt.requestFocus();

        }else {

        HashMap<Object,String> hashMap=new HashMap<>();
        hashMap.put("Dr_Name",drNAme);
        hashMap.put("Comment",comment);
        hashMap.put("RatingValue", String.valueOf(rating));
        hashMap.put("UID",smsUId);
        databaseReference.child(drNAme).setValue(hashMap);
        button.setText("Submit Successfully");
        Toast.makeText(this, "Appointment is Successfully", Toast.LENGTH_SHORT).show();}

    }


}
