package com.example.doctorfindder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static maes.tech.intentanim.CustomIntent.customType;

public class forgotpassword extends AppCompatActivity {
    public ProgressDialog progressBar;
    EditText editTextforgot;
    Button buttonforgotpassword;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        editTextforgot=(EditText)findViewById(R.id.forgotemil);
        buttonforgotpassword=(Button)findViewById(R.id.forgotbutton);
        textView=(TextView)findViewById(R.id.passwordsendconfor);
        progressBar = new ProgressDialog(this);
        databaseReference= FirebaseDatabase.getInstance().getReference("bloodproject-567cc");
        firebaseAuth = FirebaseAuth.getInstance();


        buttonforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=  editTextforgot.getText().toString().trim();
                if (email.isEmpty()){

                    editTextforgot.setError("Email Write");
                    editTextforgot.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    editTextforgot.setError("Email Not Valid");
                    editTextforgot.requestFocus();
                    return;
                }

               else {

                /*progressBar.setTitle("Forgot Password");
                progressBar.setMessage("Please wait,We are sending your Password email");
                progressBar.setIcon(R.drawable.passwordforgot);
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();*/
                forgotpass();
               }
            }
        });
    }

    public void forgotpass(){
        String email=  editTextforgot.getText().toString().trim();
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    progressBar.dismiss();
                    textView.setText("Please Chack Your Email Address");
                    //Toast.makeText(login_page2.this, "Password Send To Your Email ", Toast.LENGTH_LONG).show();

                }
                else {
                    progressBar.dismiss();
                    Toast.makeText(forgotpassword.this, "Error "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(forgotpassword.this,doctor_login.class));
            customType(forgotpassword.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
