package com.example.doctorfindder;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class appointment_listView extends AppCompatActivity {
    ListView listViewall;
    ImageView deleted;
    TextView named;
    DatabaseReference databaseReference;
    public List<appointment_ShowClass> inlist;
    public appointadptar ff;
    SharedPreferences sp;
    FirebaseAuth firebaseAuth,auth;
    FirebaseUser user;
    FirebaseDatabase firebaseDatabase;
    private ProgressDialog progressBar;
    appointment_ShowClass insdfert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_list_view);
        listViewall=(ListView)findViewById(R.id.allpostshow);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        deleted=(ImageView)findViewById(R.id.deletepost);
        named=(TextView)findViewById(R.id.namedp);
        databaseReference= FirebaseDatabase.getInstance().getReference("Appointment");
        inlist =new ArrayList<>();
        ff  =new appointadptar(appointment_listView.this,inlist);

        progressBar = new ProgressDialog(this);
        auth=FirebaseAuth.getInstance();
        progressBar.setTitle("Appointment");
        progressBar.setMessage("Please wait,Loading");
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.show();


    }


    @Override
    protected void onStart() {


        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        Query query=databaseReference.orderByChild("Email").equalTo(user.getEmail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren()){
                    appointment_ShowClass inserty  = ds.getValue(appointment_ShowClass.class);
                    inlist.add(inserty);


                }
                Collections.reverse((List<?>) inlist);
                listViewall.setAdapter(ff);

                progressBar.dismiss();

                int val = listViewall.getAdapter().getCount();
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref",  Context.MODE_PRIVATE); // 0 - for private mode
                pref.edit().putInt("valu",val).apply();
                //Toast.makeText(getApplication(),"no"+pref.getInt("valu",0),Toast.LENGTH_SHORT).show();

                //Toast.makeText(getApplication(), "error" +dataSnapshot , Toast.LENGTH_LONG).show();


                listViewall.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        for (int i = 0; i < listViewall.getCount(); i++)


                            if (position==i){
                                insdfert=inlist.get(position);

                                String ff=insdfert.getName();
                               //Toast.makeText(appointment_listView.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                            }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplication(), "error" + databaseError.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        super.onStart();

    }

    public void logout(View view) {
        alar();

    }

    public void goToMainActivity(){
        startActivity(new Intent(appointment_listView.this,doctor_login.class));
        customType(appointment_listView.this,"up-to-bottom");

        finish();
    }
    public void alar(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure,Log Out Your Account");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        FirebaseAuth.getInstance().signOut();
                        goToMainActivity();
                        sp.edit().putBoolean("logged",false).apply();
                        finish();

                        //return true;
                    }
                });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(appointment_listView.this,MainActivity.class));
            customType(appointment_listView.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void deletepostd(View view) {
        if (insdfert==null){


            Snackbar.make(view, "You Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {


            delete2();

        }
    }

    public void delete2(){
        String Name=insdfert.getName();
        String phone=insdfert.getPhone();
        String Hospital=insdfert.getHospital();
        String date=insdfert.getAdmissiondate();
        String Time=insdfert.getAdmissiontime();
        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage(" Name : " +Name+" \n phone : " +phone+" \n Hospital : "+Hospital+"\n Time  : "+Time+"\nDate  : "+date+"")
                .setTitle("Do you want to Delete")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        String sid=insdfert.getUid();

                        //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl()+"/n"+insdfert.getPhone()+"",Toast.LENGTH_LONG).show();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        Query applesQuery = ref.child("Appointment").orderByChild("Uid").equalTo(sid);

                        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                    inlist.clear();
                                    //Showdata();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }
}
