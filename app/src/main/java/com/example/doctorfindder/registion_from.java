package com.example.doctorfindder;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import static maes.tech.intentanim.CustomIntent.customType;
public class registion_from extends AppCompatActivity {
   Button date1,date2,signupbt;
    private int mYear, mMonth, mDay, mHour, mMinute;
   EditText name,email,password,retypepassword,regenumber,speciality,clinic,hospital;
   RadioGroup gender,timeshift;
    DatabaseReference reference;
    FirebaseDatabase database;
    private FirebaseAuth mAuth;
    SharedPreferences sp;
    MediaPlayer mp,trymp;
    private ProgressDialog progressBar;
   LinearLayout f1layout,f2layout,f3layout,f4layout;
    TimePickerDialog timePickerDialog;
    Animation uptodown,downtoup,getUptodown,getDowntoup;

    String format;
    Calendar calendar;
    TimePickerDialog timepickerdialog;
    private int CalendarHour, CalendarMinute;

    String gendervalue,timeshiftvalue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registion_from);
        mAuth = FirebaseAuth.getInstance();
        database=FirebaseDatabase.getInstance();
        reference=database.getReference("DrProfile");
        sp = getSharedPreferences("login",MODE_PRIVATE);
        progressBar = new ProgressDialog(this);

        //layout1
        name=(EditText)findViewById(R.id.nameid);
        email=(EditText)findViewById(R.id.emailid);
        password=(EditText)findViewById(R.id.passwordid);
        retypepassword=(EditText)findViewById(R.id.repetpasswordid);
        gender=(RadioGroup)findViewById(R.id.radioGroup1);
        //layout2
        regenumber=(EditText)findViewById(R.id.regenumberid);
        speciality=(EditText)findViewById(R.id.specialityid);
        clinic=(EditText)findViewById(R.id.clinicid);
        hospital=(EditText)findViewById(R.id.hospitalid);
        //layout3
        timeshift=(RadioGroup)findViewById(R.id.radioGroup2);
        //layout4
        date1=(Button)findViewById(R.id.timepic);
        date2=(Button)findViewById(R.id.totime);
        signupbt=(Button)findViewById(R.id.signup);

        uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);

        f1layout=(LinearLayout)findViewById(R.id.fristlayout);
        f2layout=(LinearLayout)findViewById(R.id.secondlayout);
        f3layout=(LinearLayout)findViewById(R.id.thirdlayout);
        f4layout=(LinearLayout)findViewById(R.id.fourlayout);
        f1layout.setVisibility(View.VISIBLE);
        f1layout.setAnimation(uptodown);
        mp=MediaPlayer.create(this,R.raw.rgesuccess);
        trymp=MediaPlayer.create(this,R.raw.already);

        signupbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fristdate=date1.getText().toString();
                String secenddate=date2.getText().toString();
                String drName=name.getText().toString();
                if (fristdate.isEmpty()){
                    Toast.makeText(getApplication(),"Please Select Date",Toast.LENGTH_LONG).show();
                }
                if (secenddate.isEmpty()){
                    Toast.makeText(getApplication(),"Please Select Date",Toast.LENGTH_LONG).show();
                }
                else {
                progressBar.setTitle("Registration" +drName);
                progressBar.setMessage("Please wait Sir,while we are Account Create" );
                progressBar.setIcon(R.drawable.doctor);
                //progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();
                regestion();}
            }
        });

        timeshift.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {

                    case R.id.morning:
                        timeshiftvalue="Morning";

                        break;
                    case R.id.noon:
                        timeshiftvalue="Noon";
                        break;

                    case R.id.night:
                        timeshiftvalue="Night";
                        break;
                }
            }
        });


        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {

                    case R.id.radiomale:
                        gendervalue="Male";

                        break;
                    case R.id.radiofemal:
                        gendervalue="Female";
                        break;

                    case R.id.radioother:
                        gendervalue="Other";
                        break;
                }
            }
        });

        date1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                java.util.Calendar calendar= java.util.Calendar.getInstance();
                //calendar = Calendar.getInstance();
                CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
                CalendarMinute = calendar.get(Calendar.MINUTE);


                timepickerdialog = new TimePickerDialog(registion_from.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "AM";
                                }
                                else if (hourOfDay == 12) {

                                    format = "PM";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "PM";

                                }
                                else {

                                    format = "AM";
                                }


                                date1.setText(hourOfDay + ":" + minute + format);
                            }
                        }, CalendarHour, CalendarMinute, false);
                timepickerdialog.show();

            }
        });



        date2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                java.util.Calendar calendar= java.util.Calendar.getInstance();
                //calendar = Calendar.getInstance();
                CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
                CalendarMinute = calendar.get(Calendar.MINUTE);


                timepickerdialog = new TimePickerDialog(registion_from.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "AM";
                                }
                                else if (hourOfDay == 12) {

                                    format = "PM";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "PM";

                                }
                                else {

                                    format = "AM";
                                }


                                date2.setText(hourOfDay + ":" + minute  +  format);
                            }
                        }, CalendarHour, CalendarMinute, false);
                timepickerdialog.show();

            }
        });

    }

    private void regestion() {

        String emailad=email.getText().toString().trim();
        String passwordad=password.getText().toString().trim();


       mAuth.createUserWithEmailAndPassword(emailad,passwordad).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
           @SuppressLint("ResourceAsColor")
           @Override
           public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()){
                progressBar.dismiss();
                signupbt.setText("Registration is Success");
                signupbt.setTextColor(R.color.red);
                FirebaseUser user=mAuth.getCurrentUser();
                String email=user.getEmail();
                String Uid=user.getUid();
                String drName=name.getText().toString();
                String retypepasswordad=retypepassword.getText().toString();
                String regenumberid=regenumber.getText().toString();
                String specialityad=speciality.getText().toString();
                String clinicad=clinic.getText().toString();
                String hospitalad=hospital.getText().toString();
                String passwordad=password.getText().toString().trim();
                String fristdate=date1.getText().toString();
                String secenddate=date2.getText().toString();

                HashMap<Object,String> hashMap=new HashMap<>();
                hashMap.put("DRname",drName);
                hashMap.put("Email",email);
                hashMap.put("Password",passwordad);
                hashMap.put("Uid",Uid);
                hashMap.put("RegNumberID",regenumberid);
                hashMap.put("Specialityad",specialityad);
                hashMap.put("Clinicad",clinicad);
                hashMap.put("hospitalad",hospitalad);
                hashMap.put("Gender",gendervalue);
                hashMap.put("Shift",timeshiftvalue);
                hashMap.put("Time",fristdate + " To " + secenddate);
                reference.child(drName).setValue(hashMap);
                mp.start();
                sp.edit().putBoolean("logged",true).apply();
                startActivity(new Intent(registion_from.this,appointment_listView.class));
                customType(registion_from.this,"up-to-bottom");
                finish();


            }
            else {if (task.getException() instanceof FirebaseAuthUserCollisionException){
                progressBar.dismiss();
                trymp.start();
                signupbt.setText("Already Registration");
                signupbt.setTextColor(R.color.red);

            }
            else {

                Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();
                progressBar.dismiss();
            }

            }
           }
       });


    }

    public void next1(View view) {
        String drName=name.getText().toString();
        String emailad=email.getText().toString();
        String passwordad=password.getText().toString();
        String retypepasswordad=retypepassword.getText().toString();

        if (!Patterns.EMAIL_ADDRESS.matcher(emailad).matches()){

            email.setError("Email Not Valid");
            email.requestFocus();
            return;
        }
        if (drName.isEmpty()){
            name.setError("Please enter your Name");
            name.requestFocus();
        }
        if (emailad.isEmpty()){
            email.setError("Please enter your Email address");
            email.requestFocus();
        }

        if (passwordad.isEmpty()){

            password.setError("Password Enter");
            password.requestFocus();
            return;
        }

        if (passwordad.length()<6){
            password.setError("Minimum Password 6 ");
            password.requestFocus();
            return;

        }
        if (retypepasswordad.isEmpty()){

            retypepassword.setError("RetypePassword Enter");
            retypepassword.requestFocus();
            return;
        }
        if (retypepasswordad.length()<6){
            retypepassword.setError("Minimum Password 6 ");
            retypepassword.requestFocus();
            return;

        }
        if (!passwordad.equals(retypepasswordad)){

            password.setError("Password Not Match ");
            retypepassword.setError("Password Not Match ");
            password.requestFocus();
            return;
        }

        if (gendervalue==null){
            Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show();
        }
   else {

        getDowntoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        f2layout.setVisibility(View.VISIBLE);
        f2layout.setAnimation(getDowntoup);
        f1layout.setVisibility(View.GONE);}
    }

    public void next2(View view) {
        String regenumberid=regenumber.getText().toString();
        String specialityad=speciality.getText().toString();
        String clinicad=clinic.getText().toString();
        String hospitalad=hospital.getText().toString();
        if (regenumberid.isEmpty()){
            regenumber.setError("Registration Number add");
            regenumber.requestFocus();
            return;

        }
        if (specialityad.isEmpty()){
            speciality.setError("speciality Enter");
            speciality.requestFocus();
            return;

        }
        if (clinicad.isEmpty()){
            clinic.setError("clinic Enter");
            clinic.requestFocus();
            return;

        }
        if (hospitalad.isEmpty()){
            hospital.setError("hospital Name  add");
            hospital.requestFocus();
            return;

        }
        else{
        f3layout.setVisibility(View.VISIBLE);
        f3layout.setAnimation(downtoup);
        f2layout.setVisibility(View.GONE);}
    }

    public void next3(View view) {
        if (timeshiftvalue==null){
            Toast.makeText(this, "Please Select Shift", Toast.LENGTH_SHORT).show();
      }
        else {
        getUptodown = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        f4layout.setVisibility(View.VISIBLE);
        f4layout.setAnimation(getUptodown);
        f3layout.setVisibility(View.GONE);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(registion_from.this,MainActivity.class));
            customType(registion_from.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}

