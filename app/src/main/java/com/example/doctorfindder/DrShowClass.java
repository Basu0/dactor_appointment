package com.example.doctorfindder;

public class DrShowClass  {
    private String Clinicad,DRname,Email,Gender,Password,RegNumberID,Shift,Specialityad,Time,Uid,hospitalad;

    public DrShowClass(){}

    public DrShowClass(String clinicad, String DRname, String email, String gender, String password, String regNumberID, String shift, String specialityad, String time, String uid, String hospitalad) {
        this.Clinicad = clinicad;
        this.DRname = DRname;
        this.Email = email;
        this.Gender = gender;
        this.Password = password;
        this.RegNumberID = regNumberID;
        this.Shift = shift;
        this.Specialityad = specialityad;
        this.Time = time;
        this.Uid = uid;
        this.hospitalad = hospitalad;

    }

    public String getClinicad() {
        return Clinicad;
    }

    public void setClinicad(String clinicad) {
        Clinicad = clinicad;
    }

    public String getDRname() {
        return DRname;
    }

    public void setDRname(String DRname) {
        this.DRname = DRname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getRegNumberID() {
        return RegNumberID;
    }

    public void setRegNumberID(String regNumberID) {
        RegNumberID = regNumberID;
    }

    public String getShift() {
        return Shift;
    }

    public void setShift(String shift) {
        Shift = shift;
    }

    public String getSpecialityad() {
        return Specialityad;
    }

    public void setSpecialityad(String specialityad) {
        Specialityad = specialityad;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public String getHospitalad() {
        return hospitalad;
    }

    public void setHospitalad(String hospitalad) {
        this.hospitalad = hospitalad;
    }
}
