package com.example.doctorfindder;

public class appointment_ShowClass {
    private String speciality,doctorTime,Admissiontime,Admissiondate,Name,Age,address,phone,Hospital,Dr_name,Gender,Uid;

    public appointment_ShowClass(){}



    public appointment_ShowClass(String speciality, String doctorTime, String admissiontime, String admissiondate, String name, String age, String address, String phone, String hospital, String dr_name, String gender,String uid) {
        this.speciality = speciality;
        this.doctorTime = doctorTime;
        this.Admissiontime = admissiontime;
        this.Admissiondate = admissiondate;
        this.Name = name;
        this.Age = age;
        this.address = address;
        this.phone = phone;
        this.Hospital = hospital;
        this.Dr_name = dr_name;
        this.Gender = gender;
        this.Uid = uid;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDoctorTime() {
        return doctorTime;
    }

    public void setDoctorTime(String doctorTime) {
        this.doctorTime = doctorTime;
    }

    public String getAdmissiontime() {
        return Admissiontime;
    }

    public void setAdmissiontime(String admissiontime) {
        Admissiontime = admissiontime;
    }

    public String getAdmissiondate() {
        return Admissiondate;
    }

    public void setAdmissiondate(String admissiondate) {
        Admissiondate = admissiondate;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHospital() {
        return Hospital;
    }

    public void setHospital(String hospital) {
        Hospital = hospital;
    }

    public String getDr_name() {
        return Dr_name;
    }

    public void setDr_name(String dr_name) {
        Dr_name = dr_name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }
}
