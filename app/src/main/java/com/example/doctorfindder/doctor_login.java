package com.example.doctorfindder;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static maes.tech.intentanim.CustomIntent.customType;

public class doctor_login extends AppCompatActivity {
    SharedPreferences sp;
    EditText editText1,editText2;
    Button button;
    private ProgressDialog progressBar;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_login);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        databaseReference= FirebaseDatabase.getInstance().getReference("bloodproject-567cc");
        firebaseAuth = FirebaseAuth.getInstance();
        editText1=(EditText)findViewById(R.id.emaillog);
        editText2=(EditText)findViewById(R.id.passwordlog);
        button=(Button)findViewById(R.id.loginbtn);
        progressBar = new ProgressDialog(this);
        FirebaseApp.initializeApp(this);

        if (
                sp.getBoolean("logged",false))

        {
            startActivity(new Intent(doctor_login.this,appointment_listView.class));
            customType(doctor_login.this,"up-to-bottom");

            finish();
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email=editText1.getText().toString().trim();
                String password=editText2.getText().toString().trim();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    editText1.setError("Email Not Valid");
                    editText1.requestFocus();
                    return;
                }

                if (password.isEmpty()){

                    editText2.setError("Password Null");
                    editText2.requestFocus();
                    return;
                }
                if (password.length()<6){
                    editText2.setError("Minimum Password 6 ");
                    editText2.requestFocus();
                    return;

                }
                progressBar.setTitle("Login");
                progressBar.setMessage("Please wait,We are Account Login");
                //progressBar.setIcon(R.drawable.lodingicon);
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();

                login();



            }
        });
    }

    public void login(){
        String email=editText1.getText().toString().trim();
        String password=editText2.getText().toString().trim();

        /*if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

            editText1.setError("Email Not Valid");
            editText1.requestFocus();
            return;
        }*/

        if (password.isEmpty()){

            editText2.setError("Password Null");
            editText2.requestFocus();
            return;
        }
        if (password.length()<6){
            editText2.setError("Minimum Password 6 ");
            editText2.requestFocus();
            return;

        }







        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new
                OnCompleteListener<AuthResult>() {


                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            progressBar.dismiss();
                            //mp.start();

                            sp.edit().putBoolean("logged",true).apply();
                            startActivity(new Intent(doctor_login.this,appointment_listView.class));
                            customType(doctor_login.this,"up-to-bottom");
                            finish();
                            FirebaseUser user=firebaseAuth.getCurrentUser();

                        }

                        else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                progressBar.dismiss();
                                button.setText("Already Login");
                                button.setTextColor(R.color.green);
                                //Toast.makeText(getApplication(),"",Toast.LENGTH_LONG).show();

                            }
                            else {
                                progressBar.dismiss();
                                button.setTextColor(R.color.red);
                                Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();

                            }


                        }
                    }
                });





    }

    public void regefrom(View view) {
        startActivity(new Intent(doctor_login.this,registion_from.class));
        customType(doctor_login.this,"bottom-to-up");
        finish();
    }

    public void forgotpassword(View view) {
        startActivity(new Intent(doctor_login.this,forgotpassword.class));
        customType(doctor_login.this,"bottom-to-up");
        finish();
    }
}
